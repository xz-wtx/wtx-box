package com.wtx.box.job;

import com.wtx.box.constants.BaseConstants;
import com.wtx.box.job.threadPool.ThreadPoolExecutorUtil;
import com.wtx.box.mapper.QuartzLogMapper;
import com.wtx.box.pojo.entity.QuartzJobEntity;
import com.wtx.box.pojo.entity.QuartzLogEntity;
import com.wtx.box.utils.SnowFlakeUtil;
import com.wtx.box.utils.SpringUtil;
import lombok.extern.log4j.Log4j2;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.quartz.QuartzJobBean;
import java.time.LocalDateTime;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author
 */
@Log4j2
@Async
public class ExecutionJob extends QuartzJobBean {





    /** 该处仅供参考 */
    private final static ThreadPoolExecutor EXECUTOR = ThreadPoolExecutorUtil.getPoll();


    @Override
    @SuppressWarnings("unchecked")
    protected void executeInternal(JobExecutionContext context) {
        QuartzJobEntity quartzJob = (QuartzJobEntity) context.getMergedJobDataMap().get(BaseConstants.JOB_KEY);
        // 获取spring bean
        QuartzLogMapper quartzLogMapper = SpringUtil.getBean(QuartzLogMapper.class);

        QuartzLogEntity quartzLog = new QuartzLogEntity();
        quartzLog.setId(String.valueOf(SnowFlakeUtil.getId()));
        quartzLog.setCreateTime(LocalDateTime.now());
        quartzLog.setJobName(quartzJob.getJobName());
        quartzLog.setBeanName(quartzJob.getBeanName());
        quartzLog.setMethodName(quartzJob.getMethodName());
        quartzLog.setParams(quartzJob.getParams());
        long startTime = System.currentTimeMillis();
        quartzLog.setCronExpression(quartzJob.getCronExpression());
        try {
            // 执行任务
            log.info("任务准备执行，任务名称：{}", quartzJob.getJobName());
            QuartzRunnable task = new QuartzRunnable(quartzJob.getBeanName(), quartzJob.getMethodName(),quartzJob.getParams());
            Future<?> future = EXECUTOR.submit(task);
            future.get();
            long times = System.currentTimeMillis() - startTime;
            quartzLog.setTime(times);
            // 任务状态
            quartzLog.setIsSuccess(1);
            quartzLog.setExceptionDetail("执行成功");
            log.info("任务执行完毕，任务名称：{} 总共耗时：{} 毫秒", quartzJob.getJobName(), times);
        }  catch (Exception e) {
            log.error("任务执行失败:",e);

            long times = System.currentTimeMillis() - startTime;
            quartzLog.setTime(times);
            // 任务状态 1：成功 0：失败
            quartzLog.setIsSuccess(0);
            quartzLog.setExceptionDetail("定时执行失败"+e.getMessage());
        }  finally {
            quartzLogMapper.insert(quartzLog);
        }
    }
}
