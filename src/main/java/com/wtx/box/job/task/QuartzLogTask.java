package com.wtx.box.job.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wtx.box.mapper.QuartzLogMapper;
import com.wtx.box.pojo.entity.QuartzLogEntity;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * @author WTX
 */
@Component
@Log4j2
@EnableScheduling
public class QuartzLogTask {

    @Autowired
    QuartzLogMapper quartzLogMapper;

    public void delQuartzLogLog(String day){
        LocalDate date = LocalDate.now();
        LocalDate time = date.minusDays(Integer.parseInt(day));
        quartzLogMapper.delete(new QueryWrapper<QuartzLogEntity>().le("create_time",time.atStartOfDay()));
    }
}
