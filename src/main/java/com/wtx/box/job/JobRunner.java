package com.wtx.box.job;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wtx.box.mapper.QuartzJobMapper;
import com.wtx.box.pojo.entity.QuartzJobEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author
 */
@Component
public class JobRunner implements ApplicationRunner {

    @Autowired
    private QuartzJobMapper quartzJobMapper;

    @Autowired
    private QuartzManage quartzManage;

    /**
     * 项目启动时重新激活启用的定时任务
     * @param applicationArguments /
     */
    @Override
    public void run(ApplicationArguments applicationArguments){
        printSingleColor("",31,1,"--------------------注入定时任务---------------------");
        printSingleColor("",1,1,"");
        List<QuartzJobEntity> quartzJobs = quartzJobMapper.selectList(new QueryWrapper<QuartzJobEntity>().eq("is_pause",0));
        quartzJobs.stream().forEach(x ->{
            printSingleColor("",32,1,"注入--------------------->" + x.getJobName());
            printSingleColor("",1,1,"");
        });
        quartzJobs.forEach(quartzManage::addJob);
        printSingleColor("",34,1,"--------------------定时任务注入完成---------------------");
        printSingleColor("",1,1,"");
    }
    /**
     *
     * @param pattern 前面的图案 such as "=============="
     * @param code 颜色代号：背景颜色代号(41-46)；前景色代号(31-36)
     * @param n 数字+m：1加粗；3斜体；4下划线
     * @param content 要打印的内容
     */
    public static void printSingleColor(String pattern,int code,int n,String content){
        System.out.format("%s\33[%d;%dm%s%n", pattern, code, n, content);
    }
}
