package com.wtx.box.exception;


import com.wtx.box.code.CommonEnum;

/**
 * @description: 自定义异常
 * @author: WTX
 * @create: 2020-12-26 19:16
 **/

public class GlobalException extends RuntimeException{


    private CommonEnum common;

    public CommonEnum getCommon() {
        return common;
    }

    public void setCommon(CommonEnum common) {
        this.common = common;
    }

    public GlobalException(CommonEnum  commonEnum){
        super();
        this.common=commonEnum;
    }
    public GlobalException(String msg){
        super(msg);
    }
    public GlobalException(String msg,Exception e){
        super(msg,e);
    }
}
