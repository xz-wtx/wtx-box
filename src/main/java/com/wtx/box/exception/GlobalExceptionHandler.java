package com.wtx.box.exception;

import com.wtx.box.code.StatusCodeEnum;
import com.wtx.box.utils.response.BR;
import com.wtx.box.utils.response.R;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName GlobalExceptionHandler
 * @Description WTX
 * @Date 2020/6/28
 * @Author peaks
 * @Version
 **/
@ControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public <T> R<T> runtimeExceptionHandler(NativeWebRequest request, Exception e) {
        HttpServletRequest r = (HttpServletRequest) request.getNativeRequest();

        e.printStackTrace();
        if (e instanceof HttpMessageNotReadableException) {
            return BR.genErrorResult("请求参数不能为空");
        }

        if (e instanceof RuntimeException) {
            return BR.genResult(StatusCodeEnum.FAILURE);
        }
        return BR.genResult(StatusCodeEnum.FAILURE);
    }

}
