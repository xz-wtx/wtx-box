package com.wtx.box.pojo.dto.admin;

import com.wtx.box.pojo.dto.PageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.log4j.Log4j2;

/**
 * @author WTX
 * @ClassName QuartzLogRequest
 * @Description TODO
 * @Date 2022/4/4 11:12
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Log4j2
public class AdminQuartzLogDTO extends PageDTO {
    /**
     * Bean名称
     */
    private String beanName;
    /**
     * 方法名称
     */
    private String methodName;
}
