package com.wtx.box.pojo.dto.admin;

import com.wtx.box.pojo.dto.PageDTO;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * @author WTX
 * @ClassName QuartzJobRequest
 * @Description TODO
 * @Date 2022/4/4 10:57
 * @Version 1.0
 */
@Data
@Log4j2
public class AdminQuartzJobDTO extends PageDTO {
    public String id;

    public String jobName;

    public String beanName;

}
