package com.wtx.box.pojo.dto;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * @author wtx
 * @ClassName UserLoginRequest
 * @Description TODO
 * @Date 2022/4/3 11:59
 * @Version 1.0
 */
@Data
@Log4j2
public class UserLoginDTO {

    /**
     * 微信id
     */
    private String openId;

    /**
     * 账号
     */
    private String account;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 密码
     */
    private String password;
}
