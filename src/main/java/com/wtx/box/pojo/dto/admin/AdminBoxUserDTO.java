package com.wtx.box.pojo.dto.admin;

import com.wtx.box.pojo.dto.PageDTO;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * @author wtx
 * @ClassName BoxUserDTO
 * @Description TODO
 * @Date 2022/4/5 14:07
 * @Version 1.0
 */
@Data
@Log4j2
public class AdminBoxUserDTO extends PageDTO {
}
