package com.wtx.box.pojo.dto;



import lombok.Data;

/**
 * @author wtx
 */
@Data
public class PageDTO {

    private Integer currentPage;

    private Integer pageSize;

}
