package com.wtx.box.pojo.vo.admin;

import lombok.Data;
import lombok.extern.log4j.Log4j2;


/**
 * @author 60003404
 * @ClassName BoxUserVO
 * @Description TODO
 * @Date 2022/4/5 14:09
 * @Version 1.0
 */
@Data
@Log4j2
public class AdminBoxUserVO {

    private Integer id;

    /**
     * 微信id
     */
    private String openId;

    /**
     * 账号
     */
    private String account;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 图像
     */
    private String img;
    /**
     * 邮箱
     */
    private String mail;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * token
     */
    private String token;
    /**
     * 状态：0申请,1审核通过,2审核失败
     */
    private Integer status;

    /**
     * 是否删除：0否，1是
     */
    private Boolean deleteFlag;

}
