package com.wtx.box.pojo.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

import java.util.Date;

/**
 * @author wtx
 * @ClassName LoginUserDTO
 * @Description TODO 登录对象 简单记录一下移动或PC端简单的信息
 * @Date 2022/4/2 19:50
 * @Version 1.0
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class LoginUserVO {
    /**
     * 用户id
     */
    private Integer id;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户头像
     */
    private String img;
    /**
     * 状态：0申请,1审核通过,2审核失败
     */
    private Integer status;
    /**
     * 最后登录时间
     */
    private Date lastLogin;
    /**
     * token
     */
    private String token;


}
