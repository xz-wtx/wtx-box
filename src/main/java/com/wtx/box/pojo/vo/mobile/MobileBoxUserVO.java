package com.wtx.box.pojo.vo.mobile;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * @author 60003404
 * @ClassName BoxUserMobileVO
 * @Description TODO
 * @Date 2022/4/5 14:27
 * @Version 1.0
 */
@Data
@Log4j2
public class MobileBoxUserVO {

    public Integer id;

    public String userName;
}
