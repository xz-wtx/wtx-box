package com.wtx.box.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 定时任务日志
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
@Getter
@Setter
@TableName("quartz_log")
public class QuartzLogEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * Bean名称
     */
    private String beanName;


    /**
     * cron表达式
     */
    private String cronExpression;

    /**
     * 异常详细
     */
    private String exceptionDetail;

    /**
     * 状态 1：成功 0：失败
     */
    private Integer isSuccess;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 方法名称
     */
    private String methodName;

    /**
     * 参数
     */
    private String params;

    /**
     * 耗时（毫秒）
     */
    private Long time;


    /**
     * 创建日期
     */
    private LocalDateTime createTime;
}
