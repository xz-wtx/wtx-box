package com.wtx.box.utils.generator;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.Collections;

/**
 * @ClassName MyBatisPlusGenerator
 * @Description TODO 代码生成 //https://www.jianshu.com/p/9a3066b2335a
 * @Date 2022/3/23
 * @Author WTX
 * @Version
 **/
public class MyBatisPlusGenerator {

    static String url="jdbc:mysql://localhost:3306/wtx-box?useSSL=false&useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&allowMultiQueries=true";
    static String username="root";
    static String password="root";

    public static void main(String[] args) {

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig.Builder(url,username,password).build();


        // 代码生成器
        AutoGenerator mpg =new AutoGenerator(dsc);

        String projectPath = System.getProperty("user.dir");

        // 全局配置
        GlobalConfig gc = new GlobalConfig.Builder()
                .author("wtx")
                //设置注释的日期格式
                .commentDate("yyyy-MM-dd")
                //指定输出目录,注意使用反斜杠\
                .outputDir(projectPath+ "/src/main/java")
                //使用java8新的时间类型
                .dateType(DateType.TIME_PACK)
                .build();

        // 包配置
        PackageConfig pc = new PackageConfig.Builder()
                //模块名
                .moduleName("")
                //设置父包名
                .parent("com.wtx.box")
                //设置MVC下各个模块的包名
                .entity("pojo.entity")
                .mapper("mapper")
                .service("module.admin.service")
                .serviceImpl("module.admin.service.impl")
                .controller("module.admin.controller")
                .other("other")
                //设置XML资源文件的目录
                .pathInfo(Collections.singletonMap(OutputFile.xml, "src\\main\\resources\\mapper"))
                .build();


        // 策略配置
        StrategyConfig strategy = new StrategyConfig
                .Builder()
                //表名 逗号分割
                .addInclude("box_test")
                .entityBuilder()
                //实体驼峰转换
                .naming(NamingStrategy.underline_to_camel)
                .columnNaming(NamingStrategy.underline_to_camel)
                .enableLombok()
                //Controller策略配置,开启生成@RestController控制器
                .controllerBuilder()
                .enableRestStyle()
                .build();

        //entity 配置：如user ->UserEntity.java
        strategy.entityBuilder().formatFileName("%sEntity");
        //service 配置 如：User-> UserService.java,UserServiceImpl.java
        strategy.serviceBuilder().formatServiceFileName("Admin%sService").formatServiceImplFileName("Admin%sServiceImpl");
        //mapper配置 如：User -> UserMapper.java,UserMapper.xml
        strategy.mapperBuilder().formatMapperFileName("%sMapper").formatXmlFileName("%sMapper");
        //controller配置 如：User -> UserController.java
        strategy.controllerBuilder().formatFileName("Admin%sController");


        mpg.global(gc);
        mpg.packageInfo(pc);
        mpg.strategy(strategy);

        mpg.execute();
    }
}
