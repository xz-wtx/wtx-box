package com.wtx.box.utils;

import lombok.extern.log4j.Log4j2;

/**
 * @author wtx
 * @ClassName LogUtil
 * @Description TODO
 * @Date 2022/4/4 17:05
 * @Version 1.0
 */
@Log4j2
public class LogUtil {
    /**
     *
     * @param pattern 前面的图案 such as "=============="
     * @param code 颜色代号：背景颜色代号(41-46)；前景色代号(31-36)
     * @param n 数字+m：1加粗；3斜体；4下划线
     * @param content 要打印的内容
     */
    public static void printSingleColor(String pattern,int code,int n,String content){
        System.out.format("%s\33[%d;%dm%s%n", pattern, code, n, content);
    }
}

