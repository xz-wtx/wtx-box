package com.wtx.box.utils;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.extern.log4j.Log4j2;

/**
 * 雪花算法
 * @author WTX
 */
@Log4j2
public class SnowFlakeUtil {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    /**
     * 为终端ID
     */
    private long workerId ;
    /**
     * 数据中心ID
     */
    private long datacenterId = 1;
    private   Snowflake snowflake = IdUtil.createSnowflake(workerId,datacenterId);

    public SnowFlakeUtil(){
        workerId = NetUtil.ipv4ToLong(NetUtil.getLocalhostStr());
    }

    /**
     * 成员类，SnowFlakeUtil的实例对象的保存域
     */
    private static class IdGenHolder {
        private static final SnowFlakeUtil instance = new SnowFlakeUtil();
    }

    /**
     * 外部调用获取SnowFlakeUtil的实例对象，确保不可变
     * @return
     */
    public static Snowflake get() {
        return IdGenHolder.instance.snowflake;
    }


    /**
     * 获取唯一值
     * @return
     */
    public static Long getId() {
        return SnowFlakeUtil.get().nextId();
    }





}
