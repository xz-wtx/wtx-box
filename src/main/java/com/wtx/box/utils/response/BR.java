package com.wtx.box.utils.response;

import com.wtx.box.code.StatusCodeEnum;
import java.io.Serializable;

/**
 * @ClassName R
 * @Description TODO
 * @Date 2021/3/20
 * @Author WTX
 * @Version
 **/
public class BR implements Serializable {

    public static <T> R<T> genResult(int resultCode, T data, String message) {
        R<T> result = R.newInstance();
        result.setStatus(resultCode);
        result.setMessage(message);
        result.setData(data);
        return result;
    }

    public static <T> R<T> genResult(StatusCodeEnum statusCode) {
        return genResult(statusCode.getCode(), null, statusCode.getMessage());
    }


    public static <T> R<T> genSuccessResult(T data, String message) {
        return genResult(StatusCodeEnum.SUCCESS.getCode(), data, message);
    }

    public static <T> R<T> genSuccessResult(T data) {
        return genResult(StatusCodeEnum.SUCCESS.getCode(), data, StatusCodeEnum.SUCCESS.getMessage());
    }

    public static <T> R<T> genDefSuccessResult(String message) {
        return genResult(StatusCodeEnum.SUCCESS.getCode(), null, message);
    }

    public static <T> R<T> genSuccessResult() {
        return genResult(StatusCodeEnum.SUCCESS.getCode(), null, StatusCodeEnum.SUCCESS.getMessage());
    }


    public static <T> R<T> genErrorResult(String message) {
        return genResult(StatusCodeEnum.FAILURE.getCode(), null, message);
    }

    public static <T> R<T> genErrorResult(int resultCode, String message) {
        return genResult(resultCode, null, message);
    }

}
