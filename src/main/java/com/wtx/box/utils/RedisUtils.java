package com.wtx.box.utils;

import lombok.extern.log4j.Log4j2;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author wtx
 * @ClassName RedisUtils
 * @Description TODO redis工具类
 * @Date 2022/4/3 20:41
 * @Version 1.0
 */
@Log4j2
public class RedisUtils {

    public StringRedisTemplate redisTemplate = SpringUtil.getBean(StringRedisTemplate.class);
    public RedissonClient redissonClient = SpringUtil.getBean(RedissonClient.class);


    public static  StringRedisTemplate getRedisTemplate(){
        return new RedisUtils().redisTemplate;
    }

    public static  RedissonClient getRedissonClient(){
        return new RedisUtils().redissonClient;
    }
}
