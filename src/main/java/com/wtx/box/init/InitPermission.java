package com.wtx.box.init;

import cn.hutool.crypto.SecureUtil;
import com.wtx.box.core.annotation.ApiDescribe;
import com.wtx.box.module.admin.service.AdminBoxUrlService;
import com.wtx.box.pojo.entity.BoxUrlEntity;
import com.wtx.box.utils.LogUtil;
import com.wtx.box.utils.SpringUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.AbstractHandlerMethodMapping;
import org.springframework.web.servlet.mvc.condition.PathPatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.pattern.PathPattern;

import java.time.LocalDateTime;
import java.util.*;

/**
 * @author wtx
 * @ClassName InitPermission
 * @Description TODO 读取所有requestMapper路径权限
 * @Date 2022/4/4 14:41
 * @Version 1.0
 */
@Log4j2
@Component
public class InitPermission implements CommandLineRunner {

    @Value("${server.servlet.context-path:}")
    private String contextPath;
    @Autowired
    AdminBoxUrlService boxUrlService;

    @Override
    public void run(String... args) throws Exception {
        LogUtil.printSingleColor("", 31, 1, "--------------------开始更新RequestMapper路径---------------------");
        AbstractHandlerMethodMapping<RequestMappingInfo> methodMapping =  SpringUtil.getBean(RequestMappingHandlerMapping.class);
        Map<RequestMappingInfo, HandlerMethod> mapRet = methodMapping.getHandlerMethods();

        List<BoxUrlEntity> boxUrlList=new ArrayList<>();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> m : mapRet.entrySet()) {
            RequestMappingInfo info = m.getKey();
            HandlerMethod method = m.getValue();
            PathPatternsRequestCondition p = info.getPathPatternsCondition();
            if (Objects.nonNull(p)) {
                String className = method.getMethod().getDeclaringClass().getName();
                String md =  method.getMethod().getName();
                final ApiDescribe annotation = method.getMethodAnnotation(ApiDescribe.class);

                    for (PathPattern pattern : p.getPatterns()) {
                        BoxUrlEntity boxUrl=new BoxUrlEntity();
                            boxUrl.setClassName(className);
                            boxUrl.setMethod(md);
                            boxUrl.setUrlPath(contextPath+pattern.getPatternString());
                            boxUrl.setCode(SecureUtil.md5(boxUrl.getUrlPath()));
                            boxUrl.setCreateTime(LocalDateTime.now());
                            boxUrl.setUpdateTime(LocalDateTime.now());
                            if (Objects.nonNull(annotation)){
                                boxUrl.setDeleteFlag(annotation.invalid());
                                boxUrl.setRemark(annotation.value());
                            }else{
                                boxUrl.setRemark("");
                                if (className.contains("org.springframework.boot")){
                                    boxUrl.setDeleteFlag(0);
                                }
                            }
                        boxUrlList.add(boxUrl);
                }
            }
        }
        if (!CollectionUtils.isEmpty(boxUrlList)){
            boxUrlService.insertBoxUrlList(boxUrlList);
        }
    }
}
