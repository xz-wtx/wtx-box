package com.wtx.box.module.common;

import com.wtx.box.core.annotation.ApiDescribe;
import com.wtx.box.core.annotation.NoAuth;
import com.wtx.box.module.admin.service.AdminBoxUserService;
import com.wtx.box.pojo.dto.UserLoginDTO;
import com.wtx.box.utils.response.R;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author wtx
 * @ClassName 公共登录
 * @Description TODO
 * @Date 2022/4/4 12:39
 * @Version 1.0
 */
@RestController
@RequestMapping("/")
@Log4j2
public class LoginController  {

    @Autowired
    AdminBoxUserService boxUserService;


    @ApiDescribe(value = "登录")
    @NoAuth
    @RequestMapping("login")
    public R login(@RequestBody UserLoginDTO loginRequest){

        return boxUserService.login(loginRequest);
    }
}
