package com.wtx.box.module.mobile.service;

import com.wtx.box.pojo.dto.UserLoginDTO;
import com.wtx.box.pojo.vo.LoginUserVO;
import com.wtx.box.utils.response.R;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author wtx
 * @since 2022-04-03
 */
public interface MobileBoxUserService {



    /**
     * 获取移动端用户信息
     * @param loginUser
     * @return
     */
    R getMobileUserInfo(LoginUserVO loginUser);
}
