package com.wtx.box.module.mobile.service.impl;

import com.wtx.box.mapper.BoxUserMapper;
import com.wtx.box.module.mobile.service.MobileBoxUserService;
import com.wtx.box.pojo.entity.BoxUserEntity;
import com.wtx.box.pojo.vo.LoginUserVO;
import com.wtx.box.pojo.vo.mobile.MobileBoxUserVO;
import com.wtx.box.utils.response.BR;
import com.wtx.box.utils.response.R;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author wtx
 * @since 2022-04-03
 */
@Service
public class MobileBoxUserServiceImpl implements MobileBoxUserService {


    @Autowired
    BoxUserMapper boxUserMapper;


    @Override
    public R getMobileUserInfo(LoginUserVO loginUser) {
        final BoxUserEntity userEntity = boxUserMapper.selectById(loginUser.getId());
        MobileBoxUserVO userVO=new MobileBoxUserVO();
        BeanUtils.copyProperties(userEntity,userVO);
        return BR.genSuccessResult(userVO);
    }
}
