package com.wtx.box.module.mobile.controller;

import com.wtx.box.constants.BaseConstants;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author wtx
 * @ClassName MobileBaseController
 * @Description TODO
 * @Date 2022/4/2 21:25
 * @Version 1.0
 */
@Log4j2
@Controller
@RequestMapping(BaseConstants.MOBILE)
public class MobileBaseController {


}
