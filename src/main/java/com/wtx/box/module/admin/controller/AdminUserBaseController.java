package com.wtx.box.module.admin.controller;


import com.wtx.box.core.annotation.ApiDescribe;
import com.wtx.box.core.annotation.LoginUser;
import com.wtx.box.module.admin.service.AdminBoxUserService;
import com.wtx.box.pojo.vo.LoginUserVO;
import com.wtx.box.utils.response.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wtx
 * @ClassName BoxUserController
 * @Description TODO
 * @Date 2022/4/3 15:41
 * @Version 1.0
 */
@RestController
@RequestMapping("/user")
public class AdminUserBaseController extends AdminBaseController {

    @Autowired
    AdminBoxUserService userService;

    /**
     * 获取用户信息
     *
     * @return
     */
    @ApiDescribe(value = "获取pc端用户信息")
    @RequestMapping("getUserInfo")
    public R getAdminUserInfo(@LoginUser LoginUserVO loginUser) {
        return userService.getUserInfo(loginUser);
    }

}
