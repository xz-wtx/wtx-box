package com.wtx.box.module.admin.controller;

import com.wtx.box.core.annotation.ApiDescribe;
import com.wtx.box.core.annotation.LoginUser;
import com.wtx.box.module.admin.service.AdminQuartzJobService;
import com.wtx.box.pojo.vo.LoginUserVO;
import com.wtx.box.pojo.entity.QuartzJobEntity;
import com.wtx.box.pojo.dto.admin.AdminQuartzJobDTO;
import com.wtx.box.utils.response.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 定时任务 前端控制器
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
@RestController
@RequestMapping("/quartzJob")
public class AdminQuartzJobBaseController extends AdminBaseController {
    @Autowired
    AdminQuartzJobService jobService;


    /**
     * 任务列表
     * @param quartzJob
     * @param userDTO
     * @return
     */
    @ApiDescribe(value = "定时任务列表")
    @RequestMapping("list")
    public R getJobList(@RequestBody AdminQuartzJobDTO quartzJob, @LoginUser LoginUserVO userDTO) {

        return jobService.getJobList(quartzJob,userDTO);
    }

    /**
     * 删除任务
     * @param quartzJob
     * @param userDTO
     * @return
     */
    @ApiDescribe(value = "定时任务删除")
    @RequestMapping("del")
    public R delJob(@RequestBody QuartzJobEntity quartzJob, @LoginUser LoginUserVO userDTO) {

        return jobService.delJob(quartzJob,userDTO);
    }

    /**
     * 新增/修改任务
     * @param quartzJob
     * @param userDTO
     * @return
     */
    @ApiDescribe(value = "定时任务新增/修改")
    @RequestMapping("add")
    public R addJob(@RequestBody QuartzJobEntity quartzJob, @LoginUser LoginUserVO userDTO) {

        return jobService.addJob(quartzJob,userDTO);
    }

    /**
     * 恢复任务
     * @param quartzJob
     * @param userDTO
     * @return
     */
    @ApiDescribe(value = "定时任务恢复")
    @RequestMapping("resume")
    public R resumeJob(@RequestBody QuartzJobEntity quartzJob, @LoginUser LoginUserVO userDTO) {

        return jobService.resumeJob(quartzJob,userDTO);
    }

    /**
     * 立即执行任务
     * @param quartzJob
     * @param userDTO
     * @return
     */
    @ApiDescribe(value = "定时任务立即执行")
    @RequestMapping("nowRun")
    public R runJobNow(@RequestBody QuartzJobEntity quartzJob, @LoginUser LoginUserVO userDTO) {

        return jobService.runJobNow(quartzJob,userDTO);
    }


}
