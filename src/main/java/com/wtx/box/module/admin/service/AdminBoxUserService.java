package com.wtx.box.module.admin.service;

import com.wtx.box.pojo.dto.UserLoginDTO;
import com.wtx.box.pojo.vo.LoginUserVO;
import com.wtx.box.utils.response.R;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author wtx
 * @since 2022-04-03
 */
public interface AdminBoxUserService  {

    /**
     * 用户登录
     * @param loginRequest
     * @return
     */
    R login(UserLoginDTO loginRequest);

    /**
     * 获取PC端用户信息
     * @param loginUser
     * @return
     */
    R getUserInfo(LoginUserVO loginUser);
}
