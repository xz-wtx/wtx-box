package com.wtx.box.module.admin.service;

import com.wtx.box.pojo.entity.BoxUrlEntity;
import com.wtx.box.utils.response.R;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
public interface AdminBoxUrlService {

    /**
     *跟新url路径表
     * @param boxUrlEntity
     */
    void insertBoxUrlList(List<BoxUrlEntity> boxUrlEntity);

    /**
     *  初始化url路径 加入布隆过滤器
     * @return
     */
    R initUrlPermission();
}
