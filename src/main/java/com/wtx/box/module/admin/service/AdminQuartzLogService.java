package com.wtx.box.module.admin.service;


import com.wtx.box.pojo.dto.admin.AdminQuartzLogDTO;
import com.wtx.box.utils.response.R;

/**
 * <p>
 * 定时任务日志 服务类
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
public interface AdminQuartzLogService {
    /**
     * 查询任务
     * @param quartzJobLog
     * @return
     */
    R getJobLogList(AdminQuartzLogDTO quartzJobLog);
}
