package com.wtx.box.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wtx.box.constants.BaseConstants;
import com.wtx.box.job.QuartzManage;
import com.wtx.box.mapper.QuartzJobMapper;
import com.wtx.box.module.admin.service.AdminQuartzJobService;
import com.wtx.box.pojo.dto.admin.AdminQuartzJobDTO;
import com.wtx.box.pojo.entity.QuartzJobEntity;
import com.wtx.box.pojo.vo.LoginUserVO;
import com.wtx.box.utils.response.BR;
import com.wtx.box.utils.response.R;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 * 定时任务 服务实现类
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
@Service
public class AdminQuartzJobServiceImpl  implements AdminQuartzJobService {

    @Autowired
    QuartzJobMapper quartzJobMapper;
    @Autowired
    QuartzManage quartzManage;

    @Override
    public R getJobList(AdminQuartzJobDTO quartzJob, LoginUserVO userDTO) {
        QueryWrapper<QuartzJobEntity> param = new QueryWrapper<>();
        if(StringUtils.isNotBlank(quartzJob.getJobName())){
            param.like("job_name",quartzJob.getJobName());
        }
        if(StringUtils.isNotBlank(quartzJob.getBeanName())){
            param.like("bean_name",quartzJob.getBeanName());
        }
        if(StringUtils.isNotBlank(quartzJob.getId())){
            param.like("id",quartzJob.getId());
        }
        Page<QuartzJobEntity> page=new Page<>(quartzJob.getCurrentPage(),quartzJob.getPageSize());
        Page<QuartzJobEntity> list = quartzJobMapper.selectPage(page, param);
        return BR.genSuccessResult(list);
    }


    @Override
    public R delJob(QuartzJobEntity quartzJob, LoginUserVO userDTO) {
        final QuartzJobEntity jobEntity = quartzJobMapper.selectById(quartzJob.getId());
        if (Objects.isNull(jobEntity)){
            return BR.genSuccessResult("任务不存在");
        }
        quartzManage.deleteJob(quartzJob);
        jobEntity.setIsPause(BaseConstants.YES_INT);
        jobEntity.setUpdateTime(LocalDateTime.now());
        jobEntity.setUpdateBy(userDTO.getUserName());
        quartzJobMapper.updateById(jobEntity);
        return BR.genSuccessResult("删除定时任务成功");
    }

    @Override
    public R addJob(QuartzJobEntity quartzJob, LoginUserVO userDTO) {
        if(StringUtils.isNotBlank(quartzJob.getId())){
            quartzJob.setUpdateBy(userDTO.getUserName());
            quartzJob.setUpdateTime(LocalDateTime.now());
            quartzManage.updateJobCron(quartzJob);
            quartzJobMapper.updateById(quartzJob);
            return  BR.genSuccessResult("修改定时任务成功");
        }
        quartzJob.setId(System.currentTimeMillis()+"");
        quartzJob.setIsPause(BaseConstants.NO_INT);
        quartzJob.setCreateTime(LocalDateTime.now());
        quartzJob.setCreateBy(userDTO.getUserName());
        quartzJobMapper.insert(quartzJob);
        quartzManage.addJob(quartzJob);
        return BR.genSuccessResult("新增定时任务成功");
    }

    @Override
    public R resumeJob(QuartzJobEntity quartzJob, LoginUserVO userDTO) {
        QuartzJobEntity obj = quartzJobMapper.selectOne(new QueryWrapper<QuartzJobEntity>().eq("is_pause", 1).eq("id",quartzJob.getId()));
        if (Objects.nonNull(obj)) {
            obj.setIsPause(BaseConstants.NO_INT);
            obj.setUpdateTime(LocalDateTime.now());
            obj.setUpdateBy(userDTO.getUserName());
            quartzManage.resumeJob(obj);
            quartzJobMapper.updateById(obj);
            return BR.genSuccessResult("恢复定时任务成功");
        }
        return BR.genErrorResult("任务不存在");
    }

    @Override
    public R runJobNow(QuartzJobEntity quartzJob, LoginUserVO userDTO) {
        QuartzJobEntity obj = quartzJobMapper.selectOne(new QueryWrapper<QuartzJobEntity>().eq("is_pause", 0).eq("id",quartzJob.getId()));
        if (Objects.nonNull(obj)) {
            quartzManage.runJobNow(obj);
            return BR.genSuccessResult("开始执行");
        }
        return BR.genErrorResult("任务不存在");
    }
}
