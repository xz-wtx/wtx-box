package com.wtx.box.module.admin.controller;

import com.wtx.box.constants.BaseConstants;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author WTX
 * @ClassName BaseAdminController
 * @Description TODO
 * @Date 2022/4/4 12:37
 * @Version 1.0
 */
@Log4j2
@Controller
@RequestMapping(BaseConstants.ADMIN)
public class AdminBaseController {

}
