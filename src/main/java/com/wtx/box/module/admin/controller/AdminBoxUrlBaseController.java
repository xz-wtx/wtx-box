package com.wtx.box.module.admin.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
@RestController
@RequestMapping("/url")
public class AdminBoxUrlBaseController extends AdminBaseController {

}
