package com.wtx.box.module.admin.controller;

import com.wtx.box.core.annotation.ApiDescribe;
import com.wtx.box.module.admin.service.AdminQuartzLogService;
import com.wtx.box.pojo.dto.admin.AdminQuartzLogDTO;
import com.wtx.box.utils.response.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 定时任务日志 前端控制器
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
@RestController
@RequestMapping("/quartzLog")
public class AdminQuartzLogBaseController extends AdminBaseController {
    @Autowired
    AdminQuartzLogService jobLogService;


    @ApiDescribe(value = "定时任务日志")
    @RequestMapping("list")
    public R getJobList(@RequestBody AdminQuartzLogDTO quartzJobLog) {

        return jobLogService.getJobLogList(quartzJobLog);
    }
}
