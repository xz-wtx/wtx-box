package com.wtx.box.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wtx.box.constants.BaseConstants;
import com.wtx.box.mapper.BoxUrlMapper;
import com.wtx.box.module.admin.service.AdminBoxUrlService;
import com.wtx.box.pojo.entity.BoxUrlEntity;
import com.wtx.box.utils.LogUtil;
import com.wtx.box.utils.response.BR;
import com.wtx.box.utils.response.R;
import lombok.extern.log4j.Log4j2;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
@Log4j2
@Service
public class AdminBoxUrlServiceImpl  implements AdminBoxUrlService {

    @Autowired
    BoxUrlMapper boxUrlMapper;
    @Autowired
    StringRedisTemplate redisTemplate;
    @Resource
    RedissonClient redissonClient;


    @Value("${reboot.init.permission}")
    public Boolean bool;


    @Override
    public void insertBoxUrlList(List<BoxUrlEntity> boxUrlEntityList) {

        //简单加锁
        RLock lock = redissonClient.getLock(BaseConstants.BOX_URL_LOCK);
        try {
            boolean tryLock = lock.tryLock(0, 30, TimeUnit.SECONDS);
            if (Boolean.FALSE.equals(tryLock)) {
                log.info("获取锁失败,不更新数据");
                return;
            }
            final List<BoxUrlEntity> path = boxUrlMapper.selectList(new QueryWrapper<>());
            final Map<String, BoxUrlEntity> entityMap = path.stream().collect(Collectors.toMap(BoxUrlEntity::getCode, p -> p));
            for (BoxUrlEntity boxUrl : boxUrlEntityList) {
                final BoxUrlEntity urlObj = entityMap.get(boxUrl.getCode());
                if (Objects.isNull(urlObj)) {
                    boxUrlMapper.insertEntity(boxUrl);
                } else {
                    if (!boxUrl.getClassName().equals(urlObj.getClassName())
                            || !boxUrl.getMethod().equals(urlObj.getMethod())
                            || !boxUrl.getRemark().equals(urlObj.getRemark())
                            ||boxUrl.getDeleteFlag()!=2) {
                        boxUrlMapper.update(boxUrl, new QueryWrapper<BoxUrlEntity>().eq("code", boxUrl.getCode()));
                    }
                }
            }
            LogUtil.printSingleColor("", 34, 1, "-------------更新RequestMapper路径数据完成,是否加入布隆过滤器="+bool+"------------------");
            if (bool){
                this.initUrlPermission();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {

            if (lock.isLocked()) {
                lock.unlock();
            }
        }
    }

    @Override
    public R initUrlPermission() {
        LogUtil.printSingleColor("", 31, 1, "--------------------开始初始化url到布隆过滤器---------------------");
        final List<BoxUrlEntity> path = boxUrlMapper.selectList(new QueryWrapper<BoxUrlEntity>().eq("delete_flag",0));
        //存储权限判断部分uri到布隆过滤器中-完全匹配()
        RBloomFilter<String> filters = redissonClient.getBloomFilter(BaseConstants.URI_BLOOM_FILTER_ARRAY);
        filters.delete();
        filters.tryInit(1000000L, 0.0001);
        path.forEach(p->{
            filters.add(p.getUrlPath());
        });
        LogUtil.printSingleColor("", 34, 1, "--------------------初始化url到布隆过滤器完成,数量="+filters.count()+"---------------------");
        return BR.genSuccessResult("初始化完成");
    }
}


