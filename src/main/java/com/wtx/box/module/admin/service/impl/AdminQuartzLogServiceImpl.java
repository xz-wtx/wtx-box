package com.wtx.box.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wtx.box.mapper.QuartzLogMapper;
import com.wtx.box.module.admin.service.AdminQuartzLogService;
import com.wtx.box.pojo.dto.admin.AdminQuartzLogDTO;
import com.wtx.box.pojo.entity.QuartzLogEntity;
import com.wtx.box.utils.response.BR;
import com.wtx.box.utils.response.R;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务日志 服务实现类
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
@Service
public class AdminQuartzLogServiceImpl implements AdminQuartzLogService {

    @Autowired
    QuartzLogMapper quartzLogMapper;

    @Override
    public R getJobLogList(AdminQuartzLogDTO quartzJobLog) {
        QueryWrapper<QuartzLogEntity> param = new QueryWrapper<>();
        if(StringUtils.isNotBlank(quartzJobLog.getMethodName())){
            param.like("method_name",quartzJobLog.getMethodName());
        }
        param.orderByDesc("create_time");
        Page<QuartzLogEntity> page=new Page<>(quartzJobLog.getCurrentPage(),quartzJobLog.getPageSize());
        Page<QuartzLogEntity> list = quartzLogMapper.selectPage(page, param);

        return BR.genSuccessResult(list);
    }
}
