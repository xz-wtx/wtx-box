package com.wtx.box.module.admin.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wtx.box.constants.BaseConstants;
import com.wtx.box.mapper.BoxUserMapper;
import com.wtx.box.module.admin.service.AdminBoxUserService;
import com.wtx.box.pojo.dto.UserLoginDTO;
import com.wtx.box.pojo.entity.BoxUserEntity;
import com.wtx.box.pojo.vo.LoginUserVO;
import com.wtx.box.pojo.vo.admin.AdminBoxUserVO;
import com.wtx.box.utils.JwtUtils;
import com.wtx.box.utils.response.BR;
import com.wtx.box.utils.response.R;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author wtx
 * @since 2022-04-03
 */
@Service
public class AdminBoxUserServiceImpl implements AdminBoxUserService {

    @Autowired
    RedisTemplate<String, String> redisTemplate;
    @Autowired
    BoxUserMapper boxUserMapper;

    @Transactional
    @Override
    public R login(UserLoginDTO loginRequest) {

        final QueryWrapper<BoxUserEntity> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(loginRequest.getOpenId())){
            queryWrapper.eq("open_id",loginRequest.getOpenId());
        }else{
            if (StringUtils.isNotBlank(loginRequest.getAccount())){
                queryWrapper.eq("account",loginRequest.getAccount());
            }else{
                if (StringUtils.isBlank(loginRequest.getPhone())){
                    return BR.genErrorResult("账号密码不正确");
                }
                queryWrapper.eq("phone",loginRequest.getPhone());
            }
            if (StringUtils.isBlank(loginRequest.getPassword())||loginRequest.getPassword().length()>6){
                return BR.genErrorResult("密码不能小于6位");
            }
            queryWrapper.eq("password",loginRequest.getPassword());
        }
        final BoxUserEntity boxUser = boxUserMapper.selectOne(queryWrapper);
        if (Objects.isNull(boxUser)){
            return BR.genErrorResult("用户不存在");
        }
        if (boxUser.getDeleteFlag()){
            return BR.genErrorResult("用户不可用");
        }

        //移动端重写登录方法,封装的登录信息字段要保证一样
        LoginUserVO loginUser=new LoginUserVO(){{
            setId(boxUser.getId());
            setUserName(boxUser.getUserName());
            setImg(boxUser.getImg());
            setStatus(boxUser.getStatus());
        }};


        final String jwt = JwtUtils.createJWT(loginUser);
        loginUser.setToken(jwt);
        loginUser.setLastLogin(new Date());

        //删除历史token
        redisTemplate.delete(BaseConstants.USER_TOKEN+boxUser.getToken());

        //写入数据库
        boxUser.setLastLogin(LocalDateTime.now());
        boxUser.setToken(loginUser.getToken());
        boxUserMapper.updateById(boxUser);

        //新加入token
        redisTemplate.opsForValue().set (BaseConstants.USER_TOKEN+loginUser.getToken(),JSONObject.toJSONString(loginUser),60*30, TimeUnit.SECONDS);

        return BR.genSuccessResult(loginUser);
    }


    @Override
    public R getUserInfo(LoginUserVO loginUser) {
        final BoxUserEntity userEntity = boxUserMapper.selectById(loginUser.getId());
        AdminBoxUserVO userVO=new AdminBoxUserVO();
        BeanUtils.copyProperties(userEntity,userVO);
        return BR.genSuccessResult(userVO);
    }
}
