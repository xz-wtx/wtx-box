package com.wtx.box.module.admin.service;

import com.wtx.box.pojo.dto.admin.AdminQuartzJobDTO;
import com.wtx.box.pojo.entity.QuartzJobEntity;
import com.wtx.box.pojo.vo.LoginUserVO;
import com.wtx.box.utils.response.R;

/**
 * <p>
 * 定时任务 服务类
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
public interface AdminQuartzJobService {
    /**
     * 查询任务
     * @param quartzJob
     * @return
     */
    R getJobList(AdminQuartzJobDTO quartzJob, LoginUserVO userDTO);

    /**
     * 删除任务
     * @param quartzJob
     * @return
     */
    R delJob(QuartzJobEntity quartzJob, LoginUserVO userDTO);

    /**
     * 添加修改
     * @param quartzJob
     * @return
     */
    R addJob(QuartzJobEntity quartzJob, LoginUserVO userDTO);

    /**
     * 恢复任务
     * @param quartzJob
     * @return
     */
    R resumeJob(QuartzJobEntity quartzJob, LoginUserVO userDTO);

    /**
     *立即执行
     * @param quartzJob
     * @return
     */
    R runJobNow(QuartzJobEntity quartzJob, LoginUserVO userDTO);
}
