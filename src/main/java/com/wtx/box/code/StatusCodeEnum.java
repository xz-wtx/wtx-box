package com.wtx.box.code;

/**
 * @ClassName ChicmaxPromptCode
 * @Description 全局返回错误码
 * @Date 2020/6/29
 * @Author WTX
 * @Version
 **/
public enum StatusCodeEnum {

    //公共
    SUCCESS(200,""),
    FAILURE(500,"服务内部错误"),
    ERROR_404(404,"Not Found"),
    ERROR_415(415,"Unsupported Media Type"),
    ;
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private StatusCodeEnum() {
    }

    private StatusCodeEnum(int code) {
        this.code = code;
    }

    private StatusCodeEnum(int code, String msg) {
        this.code = code;
        this.message = msg;
    }
}
