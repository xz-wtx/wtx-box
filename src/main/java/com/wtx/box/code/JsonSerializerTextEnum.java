package com.wtx.box.code;

import com.alibaba.fastjson.JSONObject;

/**
 * @author wtx
 * @ClassName JsonSerializerTextEnum
 * @Description TODO
 * @Date 2022/4/2 17:47
 * @Version 1.0
 */
public enum JsonSerializerTextEnum {

    /**
     * 默认null
     */
    NO(null),
    /**
     * 默认null
     */
    Null(null),
    /**
     * 默认空对象
     */
    Object(new JSONObject()),
    /**
     * 默认空字符
     */
    String(""),
    /**
     * 默认0
     */
    Number(0),


    ;
      private Object text;

    JsonSerializerTextEnum(Object text) {
        this.text = text;
    }

    public Object getText() {
        return text;
    }

    public void setText(Object text) {
        this.text = text;
    }
}
