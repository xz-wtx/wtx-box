package com.wtx.box.code;

/**
 * @author wtx
 * @Description 所有业务异常的枚举
 * @date 2016年11月12日 下午5:04:51
 */
public enum CommonEnum {

	/**
	 * 字典
	 */
	NULL_EXCEPTION(1000, "空指针"),

	NO_DATA(1000, "未查询到数据"),

	PARAM_ERROR(1001, "参数错误"),

	NO_PERMISSION(1002, "无权限"),

	DEL_JOB_FAIL(1003,"删除定时任务失败"),

	CREATE_JOB_FAIL(1004,"创建定时任务失败"),

	UPDATE_JOB_FAIL(1005,"更新定时任务失败"),

	PY_PARSING_ERROR(2000,"拼音解析错误"),

	SAP_QUERY_ERROR(2100,"SAP认款总账科目查询失败"),

	PY_NO_ACCOUNTPERIOD(2200,"未查询账期"),

	PY_NO_PAYMENT_DAYS(2200,"该认款账期已关闭，认款申请被拒绝"),

	SAP_CREDIT_ERROR(2200,"SAP认款入账失败"),

	SAP_QUERY_EXCUTE_ERROR(2200,"SAP认款查询处理时出现未知异常"),

	SAP_CREDIT_EXCUTE_ERROR(2300,"SAP认款入账处理时出现未知异常"),

	SAP_TRANSFER_ERROR(2400,"SAP转款处理时出现未知异常"),

	SAP_TRANSFER_EXCUTE_ERROR(2450,"SAP转款处理失败！"),

	INTERNAL_SERVER_ERROR(5000,"服务器内部错误"),

	COMMON_MSG_ERROR(1000,"");



	private int code;
	public String msg;

	CommonEnum(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public CommonEnum err(String msg){
		COMMON_MSG_ERROR.setMsg(msg);
		return this;
	}

}
