package com.wtx.box.mapper;

import com.wtx.box.pojo.entity.QuartzLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务日志 Mapper 接口
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
public interface QuartzLogMapper extends BaseMapper<QuartzLogEntity> {

}
