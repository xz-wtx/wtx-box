package com.wtx.box.mapper;

import com.wtx.box.pojo.entity.BoxUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author wtx
 * @since 2022-04-03
 */
public interface BoxUserMapper extends BaseMapper<BoxUserEntity> {



}
