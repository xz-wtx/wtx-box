package com.wtx.box.mapper;

import com.wtx.box.pojo.entity.BoxUrlEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wtx
 * @since 2022-04-04
 */
public interface BoxUrlMapper extends BaseMapper<BoxUrlEntity> {

    /**
     * 插入数据
     * @param boxUrl
     */
    void insertEntity(@Param("boxUrl") BoxUrlEntity boxUrl);
}
