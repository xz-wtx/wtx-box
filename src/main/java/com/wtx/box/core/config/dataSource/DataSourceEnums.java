package com.wtx.box.core.config.dataSource;

/**
 * @ClassName DataSourceEnums
 * @Description TODO
 * @Date 2021/8/27
 * @Author WTX
 * @Version
 **/
public enum DataSourceEnums {

    box("boxDataSource");

    private String value;

    DataSourceEnums(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
