package com.wtx.box.core.config.dataSource;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @ClassName DataSourceAspect
 * @Description TODO
 * @Date 2021/8/26
 * @Author WTX
 * @Version
 **/
@Component
@Aspect
public class DataSourceAspect {

    @Before("execution(* com.wtx.box.mapper..*.*(..))")
    public void setDataSourceTask() {
        DataSourceContextHolder.setDataSource(DataSourceEnums.box.getValue());
    }

}
