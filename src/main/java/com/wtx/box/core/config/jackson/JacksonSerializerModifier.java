package com.wtx.box.core.config.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;
import com.wtx.box.code.JsonSerializerTextEnum;
import com.wtx.box.core.annotation.JsonNoNull;
import lombok.extern.log4j.Log4j2;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * @author wtx
 * @ClassName JacksonSerializerModifier
 * @Description TODO 初始默认值
 * @Date 2022/4/2 11:18
 * @Version 1.0
 */
@Log4j2
public class JacksonSerializerModifier extends BeanSerializerModifier {

    @Override
        public List<BeanPropertyWriter> changeProperties(SerializationConfig config, BeanDescription beanDesc, List<BeanPropertyWriter> beanProperties) {
            final JsonInclude annotation = beanDesc.getClassInfo().getAnnotation(JsonInclude.class);
            if (Objects.isNull(annotation)) {
                for (BeanPropertyWriter writer : beanProperties) {
                    JsonNoNull jsonNoNull = writer.getAnnotation(JsonNoNull.class);
                    if (Objects.nonNull(jsonNoNull)){
                        if (!"".equals(jsonNoNull.msg())){
                            jsonNoNull.text().setText(jsonNoNull.msg());
                        }
                        writer.assignNullSerializer(new NullCustomSerializer(jsonNoNull.text()));
                    }

                    final JsonInclude rowAnnotation = writer.getAnnotation(JsonInclude.class);
                    if (Objects.isNull(rowAnnotation)){
                        final JavaType javaType = writer.getType();
                        final Class<?> rawClass = javaType.getRawClass();
                        if (javaType.isArrayType() || javaType.isCollectionLikeType()) {
                            writer.assignNullSerializer(new NullListJsonSerializer());
                        } else if (Number.class.isAssignableFrom(rawClass) && (rawClass.getName().startsWith("java.lang") || rawClass.getName().startsWith("java.math"))) {
                            writer.assignNullSerializer(new NullNumberSerializer());
                        } else if (Boolean.class.equals(rawClass)) {
                            writer.assignNullSerializer(new NullBooleanSerializer());
                        } else if (String.class.equals(rawClass)) {
                            writer.assignNullSerializer(new NullStringSerializer());
                        }
                    }

                }
            }
            return beanProperties;
        }
    }


class NullListJsonSerializer extends JsonSerializer<Object> {
  @Override
  public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
      jgen.writeStartArray();
      jgen.writeEndArray();
  }
}

  class NullNumberSerializer extends JsonSerializer<Object> {
    @Override
    public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeNumber(0);
    }
}

  class NullBooleanSerializer extends JsonSerializer<Object> {
    @Override
    public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeBoolean(false);
    }
}

  class NullStringSerializer extends JsonSerializer<Object> {
      @Override
      public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
          jgen.writeString("");
      }
  }

class NullCustomSerializer extends JsonSerializer<Object> {
    JsonSerializerTextEnum jsonSerializerEnum;
    public NullCustomSerializer(JsonSerializerTextEnum jsonEnum){
        jsonSerializerEnum=jsonEnum;
    }
    @Override
    public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeObject(jsonSerializerEnum.getText());
    }
}