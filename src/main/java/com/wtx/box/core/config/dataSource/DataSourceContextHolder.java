package com.wtx.box.core.config.dataSource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @ClassName DataSourceType
 * @Description TODO
 * @Date 2021/8/26
 * @Author WTX
 * @Version
 **/
@Slf4j
public class DataSourceContextHolder extends AbstractRoutingDataSource {

    public static final ThreadLocal<String> contextHolder = new ThreadLocal<>();

    /**
     * 获取数据源
     */
    @Override
    protected Object determineCurrentLookupKey() {
        log.info("当前选择的数据源是:" + contextHolder.get());
        return contextHolder.get();
    }

    /**
     * 设置数据源
     */
    public static void setDataSource(String db) {
        contextHolder.set(db);
    }

    /**
     * 取得当前数据源
     *
     * @return
     */
    public static String getDataSource() {
        return contextHolder.get();
    }

    /**
     * 清除上下文数据
     */
    public static void clear() {
        contextHolder.remove();
    }
}
