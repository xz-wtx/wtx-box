package com.wtx.box.core.config.jackson;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

/**
 * @author wtx
 * @ClassName JsonCustomObjectMapperFactoryConfig
 * @Description TODO jackson序列化参数未null时，赋值默认值
 * @Date 2022/4/2 11:30
 * @Version 1.0
 */
@Component
@Log4j2
public class JsonCustomObjectMapperFactoryConfig implements FactoryBean<ObjectMapper> {

    @Override
    public ObjectMapper getObject() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializerFactory(objectMapper.getSerializerFactory()
                .withSerializerModifier(new JacksonSerializerModifier()))
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        //ava 8 date/time type `java.time.LocalDateTime` not supported by obj转对象报错
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                    .registerModule(new JavaTimeModule());
        return objectMapper;
    }

    @Override
    public Class<?> getObjectType() {
        return ObjectMapper.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
