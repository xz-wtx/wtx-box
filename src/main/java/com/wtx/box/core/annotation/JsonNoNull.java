package com.wtx.box.core.annotation;

import com.wtx.box.code.JsonSerializerTextEnum;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wtx
 * @ClassName loginAuth
 * @Description TODO 指定字段返回结果： text和msg 二选一
 * @Date 2022/3/28 13:43
 * @Version 1.0
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonNoNull {

   /**
    * 类型返回
    * @return
    */
   JsonSerializerTextEnum text() default JsonSerializerTextEnum.NO;

   /**
    * 自定义返回
    * @return
    */
   String msg() default "";
}
