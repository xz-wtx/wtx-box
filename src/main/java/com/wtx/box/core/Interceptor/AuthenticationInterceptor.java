package com.wtx.box.core.Interceptor;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.wtx.box.core.annotation.NoAuth;
import com.wtx.box.constants.BaseConstants;
import com.wtx.box.pojo.vo.LoginUserVO;
import com.wtx.box.utils.JwtUtils;
import com.wtx.box.utils.RedisUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;


/**
 * @description: TODO 拦截api请求
 * @author: WTX
 * @create: 2021-01-18 14:59
 **/

@Log4j2
public class AuthenticationInterceptor implements HandlerInterceptor {

    //当前用户标识
    public static String CURRENT_USER="current_user";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        response.setHeader("Access-Control-Allow-Headers", BaseConstants.AUTH_TOKEN);
        String url = request.getRequestURL().toString();
        if (url.contains("/error")){
            return true;
        }
        String token = request.getHeader(BaseConstants.AUTH_TOKEN);
        log.info("Token:{},url:{}", token, url);
        if (handler instanceof HandlerMethod) {
            final NoAuth loginAuth = ((HandlerMethod) handler).getMethodAnnotation(NoAuth.class);
            if (Objects.nonNull(loginAuth)) {
                return true;
            }

            if (StringUtils.isBlank(token)) {
                responseBody(response, 401, "请先登录");
                return false;
            } else {
                try {
                    LoginUserVO fromToken = JwtUtils.parseJWT(token, LoginUserVO.class);
                    Boolean aBoolean = RedisUtils.getRedisTemplate().hasKey(BaseConstants.USER_TOKEN+token);
                    if (Boolean.FALSE.equals(aBoolean)){
                        responseBody(response, 401, "token已失效");
                        return false;
                    }
                    request.setAttribute(CURRENT_USER, fromToken);
                } catch (Exception e) {
                    e.printStackTrace();
                    if (e instanceof TokenExpiredException){
                        responseBody(response, 401, e.getMessage());
                        return false;
                    }
                    responseBody(response, 500,e.getMessage());
                    return false;
                }
            }
        }

        return true;

    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }


    public void responseBody(HttpServletResponse response, Integer status, String message) {
        response.setCharacterEncoding("UTF-8");
        response.setStatus(status);
        try {
            response.getWriter().write(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}