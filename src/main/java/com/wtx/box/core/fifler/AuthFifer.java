package com.wtx.box.core.fifler;

import com.wtx.box.constants.BaseConstants;
import com.wtx.box.utils.RedisUtils;
import lombok.extern.log4j.Log4j2;
import org.redisson.api.RBloomFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wtx
 * @ClassName AuthFifer
 * @Description TODO
 * @Date 2022/4/4 17:59
 * @Version 1.0
 */
@Log4j2
@Configuration
@Order(1)
@WebFilter(filterName = "firstFilter",urlPatterns = "/*")
public class AuthFifer implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        final RBloomFilter<Object> filter = RedisUtils.getRedissonClient().getBloomFilter(BaseConstants.URI_BLOOM_FILTER_ARRAY);
        String url = request.getRequestURI();
        final boolean b = filter.contains(url);
        if (!b){
            resp.sendError(400,"访问路径有误");
            return;
        }
        filterChain.doFilter(request, resp);
    }
}
