package com.wtx.box;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 该项目使用
 * @author wtx
 */
@SpringBootApplication
public class AdminBoxApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminBoxApplication.class, args);
    }

}
