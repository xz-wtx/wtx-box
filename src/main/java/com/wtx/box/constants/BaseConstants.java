package com.wtx.box.constants;

import lombok.extern.log4j.Log4j2;

/**
 * @author wtx
 * @ClassName BaseConstants
 * @Description TODO 常量
 * @Date 2022/4/2 19:45
 * @Version 1.0
 */
@Log4j2
public class BaseConstants {

    //PC端请求前缀
    public static final String ADMIN="admin";
    //移动端请求前缀
    public static final String MOBILE="mobile";

    //Header 名称
    public static String AUTH_TOKEN="auth_token";

    //redis文件名称
    public static String USER_TOKEN="user_token:";

    //job标识
    public static final String JOB_KEY = "BOX_JOB_KEY";

    //加锁标识
    public static final String BOX_URL_LOCK = "BOX_URL_LOCK";
    /**
     * 布隆过滤器权限值
     */
    public static final String URI_BLOOM_FILTER_ARRAY = "UriBloomFilterArray";

    public static Integer NO_INT=0;

    public static Integer YES_INT=1;

    public static String NO_STRING="0";

    public static String YES_STRING="1";
}
